<?php

$EM_CONF['mentor_finder'] = [
    'title' => 'Mentor Finder',
    'description' => 'Mit dieser Extension ist es Mentees möglich anhand Suchkriterien den passenden Mentor zu finden.',
    'category' => 'plugin',
    'author' => 'Heiko Wollersheim',
    'author_email' => 'heiko.wollersheim@eifel-online.com',
    'author_company' => 'Gesellschaft für Informatik e.V.',
    'state' => 'stable',
    'internal' => '',
    'modify_tables' => '',
    'uploadfolder' => '0',
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '3.0.6',
    'constraints' => [
        'depends' => [
            'typo3' => '11.5.0-11.5.99',
            'sf_register' => '11.0.0-11.9.99'
        ],
        'conflicts' => [],
        'suggests' => []
    ],
    'autoload' => [
        'psr-4' => [
            'GI\\MentorFinder\\' => 'Classes'
        ]
    ]
];