<?php
namespace GI\MentorFinder\Controller;

use Psr\Http\Message\ResponseInterface;
use Evoweb\SfRegister\Domain\Model\FrontendUser;
use TYPO3\CMS\Core\Http\HtmlResponse;
/**
 * An frontend user create controller
 */
class FeuserCreateController extends \Evoweb\SfRegister\Controller\FeuserCreateController
{
    public function formAction(FrontendUser $user = null): ResponseInterface
    {
        $setupResponse = $this->setupCheck();

        $originalRequest = $this->request->getOriginalRequest();
        if ($originalRequest !== null && $originalRequest->hasArgument('user')) {
            /** @var FrontendUser $userData */
            $userData = $this->request->hasArgument('user') ?
                $this->request->getArgument('user') :
                $originalRequest->getArgument('user');
            if (isset($userData['uid'])) {
                unset($userData['uid']);
            }
        }

        if ($user) {
            $this->eventDispatcher->dispatch(new CreateFormEvent($user, $this->settings));
            $this->view->assign('user', $user);
        }

        return $setupResponse ?? new HtmlResponse($this->view->render());
    }
}
