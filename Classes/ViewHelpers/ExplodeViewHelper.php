<?php
namespace GI\MentorFinder\ViewHelpers;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use function GuzzleHttp\Psr7\str;

class ExplodeViewHelper extends \TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper
{
    /**
     * initialize arguments
     */
    public function initializeArguments()
    {
        $this->registerArgument('string', 'string', 'String to split', true);
        $this->registerArgument('delimiter', 'string', 'Delimiter', true);
    }

    /**
     * render
     */
    public function render()
    {
        $returnArray = explode($this->arguments['delimiter'], $this->arguments['string']);
        return $returnArray;
    }
}