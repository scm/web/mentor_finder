<?php
namespace GI\MentorFinder\ViewHelpers;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

class CategoryViewHelper extends \TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper
{
    /**
     * initialize arguments
     */
    public function initializeArguments()
    {
        $this->registerArgument('pageId', 'int', 'The PID for shown categories', true);
    }

    /**
     * render
     */
    public function render()
    {
        $objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\ObjectManager::class);
        $categoryRepository = $objectManager->get(\GI\MentorFinder\Domain\Repository\CategoryRepository::class);
        $categories = $categoryRepository->findByPid($this->arguments['pageId']);

        $returnArray = [];
        foreach ($categories as $category) {
            $returnArray[$category->getUid()] = $category->getTitle();
        }

        return $returnArray;
    }
}
