<?php
namespace GI\MentorFinder\Domain\Model;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

/**
 * Demand
 */
class Demand extends \TYPO3\CMS\Extbase\DomainObject\AbstractValueObject
{
    /**
     * Search word
     *
     * @var string
     */
    protected $searchWord = "";

    /**
     * Category
     *
     * @var int
     */
    protected $category = 0;

    /**
     * Show all
     *
     * @var int
     */
    protected $showAll = 0;

    /**
     * Zip
     *
     * @var string
     */
    protected $zip = '';

    /**
     * Returns the searchWord
     *
     * @return string $searchWord
     */
    public function getSearchWord(): string
    {
        return $this->searchWord;
    }

    /**
     * Sets the searchWord
     *
     * @param string $searchWord
     * @return void
     */
    public function setSearchWord($searchWord)
    {
        $this->searchWord = $searchWord;
    }

    /**
     * Returns the category
     *
     * @return int $category
     */
    public function getCategory(): int
    {
        return $this->category;
    }

    /**
     * Sets the category
     *
     * @param int $category
     * @return void
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * Returns the showAll
     *
     * @return int $showAll
     */
    public function getShowAll(): int
    {
        return $this->showAll;
    }

    /**
     * Sets the showAll
     *
     * @param int $showAll
     * @return void
     */
    public function setShowAll($showAll)
    {
        $this->showAll = $showAll;
    }

    /**
     * Returns the zip
     *
     * @return string $zip
     */
    public function getZip(): string
    {
        return $this->zip;
    }

    /**
     * Sets the zip
     *
     * @param string $zip
     * @return void
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
    }
}
