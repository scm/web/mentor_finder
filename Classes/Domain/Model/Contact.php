<?php
namespace GI\MentorFinder\Domain\Model;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

/**
 * Contact
 */
class Contact extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * Id
     *
     * @var integer
     */
    protected $id;

    /**
     * First name
     *
     * @var string
     */
    protected $firstName;

    /**
     * Last name
     *
     * @var string
     */
    protected $lastName;

    /**
     * E-Mail
     *
     * @var string
     */
    protected $email;

    /**
     * Short description
     *
     * @var string
     */
    protected $shortDescription;

    /**
     * Required help
     *
     * @var string
     */
    protected $requiredHelp;

    /**
     * Gtc
     *
     * @var int
     */
    protected $gtc;

    /**
     * Type
     *
     * @var string
     */
    protected $type;

    /**
     * Mentor
     *
     * @var int
     */
    protected $mentor;


    /**
     * Returns the firstName
     *
     * @return string $firstName
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Sets the id
     *
     * @param string $id
     * @return void
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Returns the id
     *
     * @return string $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the firstName
     *
     * @param string $firstName
     * @return void
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * Returns the lastName
     *
     * @return string $lastName
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Sets the lastName
     *
     * @param string $lastName
     * @return void
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * Returns the email
     *
     * @return string $email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Sets the email
     *
     * @param string $email
     * @return void
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Returns the shortDescription
     *
     * @return string $shortDescription
     */
    public function getShortDescription()
    {
        return $this->shortDescription;
    }

    /**
     * Sets the shortDescription
     *
     * @param string $shortDescription
     * @return void
     */
    public function setShortDescription($shortDescription)
    {
        $this->shortDescription = $shortDescription;
    }

    /**
     * Returns the requiredHelp
     *
     * @return string $requiredHelp
     */
    public function getRequiredHelp()
    {
        return $this->requiredHelp;
    }

    /**
     * Sets the requiredHelp
     *
     * @param string $requiredHelp
     * @return void
     */
    public function setRequiredHelp($requiredHelp)
    {
        $this->requiredHelp = $requiredHelp;
    }

    /**
     * Returns the gtc
     *
     * @return int $gtc
     */
    public function getGtc()
    {
        return $this->gtc;
    }

    /**
     * Sets the gtc
     *
     * @param int $gtc
     * @return void
     */
    public function setGtc($gtc)
    {
        $this->gtc = $gtc;
    }

    /**
     * Returns the type
     *
     * @return int $type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Sets the type
     *
     * @param int $type
     * @return void
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Returns the mentor
     *
     * @return int $mentor
     */
    public function getMentor()
    {
        return $this->mentor;
    }

    /**
     * Sets the mentor
     *
     * @param int $mentor
     * @return void
     */
    public function setMentor($mentor)
    {
        $this->mentor = $mentor;
    }
}
