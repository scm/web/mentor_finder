<?php
namespace GI\MentorFinder\Domain\Model;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

/**
 * Frontend user
 */
class FrontendUser extends \Evoweb\SfRegister\Domain\Model\FrontendUser
{
    /**
     * @var ObjectStorage<Category>
     */
    protected $categories = null;

    /**
     * Job description
     *
     * @var string
     */
    protected $jobDescription;

    /**
     * Short CV
     *
     * @var string
     */
    protected $shortCv;

    /**
     * Qualification
     *
     * @var string
     */
    protected $qualification;

    /**
     * Resources
     *
     * @var string
     */
    protected $resources;

    /**
     * Requests
     *
     * @var string
     */
    protected $requests;

    /**
     * Hidden
     *
     * @var string
     */
    protected $hidden;

    public function __construct() {
        parent::__construct();
        $this->categories = new ObjectStorage();
    }

    /**
     * Returns the jobDescription
     *
     * @return string $jobDescription
     */
    public function getJobDescription(): string
    {
        return $this->jobDescription;
    }

    /**
     * Sets the jobDescription
     *
     * @param string $jobDescription
     * @return void
     */
    public function setJobDescription($jobDescription)
    {
        $this->jobDescription = $jobDescription;
    }

    /**
     * Returns the shortCv
     *
     * @return string $shortCv
     */
    public function getShortCv(): string
    {
        return $this->shortCv;
    }

    /**
     * Sets the shortCv
     *
     * @param string $shortCv
     * @return void
     */
    public function setShortCv($shortCv)
    {
        $this->shortCv = $shortCv;
    }

    /**
     * Returns the qualification
     *
     * @return string $qualification
     */
    public function getQualification(): string
    {
        return $this->qualification;
    }

    /**
     * Sets the qualification
     *
     * @param string $qualification
     * @return void
     */
    public function setQualification($qualification)
    {
        $this->qualification = $qualification;
    }

    /**
     * Returns the resources
     *
     * @return string $resources
     */
    public function getResources(): string
    {
        return $this->resources;
    }

    /**
     * Sets the resources
     *
     * @param string $resources
     * @return void
     */
    public function setResources($resources)
    {
        $this->resources = $resources;
    }

    /**
     * Returns the requests
     *
     * @return string $requests
     */
    public function getRequests(): string
    {
        return $this->requests;
    }

    /**
     * Sets the requests
     *
     * @param string $requests
     * @return void
     */
    public function setRequests($requests)
    {
        $this->requests = $requests;
    }

    /**
     * Returns the hidden stat
     *
     * @return string $hidden
     */
    public function getHidden(): string
    {
        return $this->hidden;
    }

    /**
     * Sets the hidden stat
     *
     * @param string $hidden
     * @return void
     */
    public function setHidden($hidden)
    {
        $this->hidden = $hidden;
    }

    /**
     * @param Category $category
     * @return void
     */
    public function addCategory(Category $category) {
        $this->categories->attach($category);
    }

    /**
     * @param Category $categoryToRemove
     * @return void
     */

    public function removeCategory(Category $categoryToRemove) {
        $this->categories->detach($categoryToRemove);
    }

    /**
     * Returns the categories
     *
     * @return ObjectStorage<Category>
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Sets the categories
     *
     * @param ObjectStorage<Category> $categories
     * @return void
     */
    public function setCategories($categories)
    {
        $this->categories = $categories;
    }
}
