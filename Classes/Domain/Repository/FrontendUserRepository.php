<?php
namespace GI\MentorFinder\Domain\Repository;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

/**
 * FrontendUseRepository
 */
class FrontendUserRepository extends \Evoweb\SfRegister\Domain\Repository\FrontendUserRepository
{
    /**
     * Search
     *
     * @param \GI\MentorFinder\Domain\Model\Demand $demand
     * @return \TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findAllByDemand(\GI\MentorFinder\Domain\Model\Demand $demand)
    {
        $query = $this->createQuery();
        $constraints = [];

        $constraints['hidden'] = $query->equals('hidden', 0);

        if($demand->getSearchWord()) {
            $constraints['searchword'] = $query->logicalOr(
                [
                    $query->like('job_description', '%'.$demand->getSearchWord().'%'),
                    $query->like('qualification', '%'.$demand->getSearchWord().'%'),
                    $query->like('resources', '%'.$demand->getSearchWord().'%')
                ]
            );
        }

        if($demand->getCategory()) {
            $constraints['categories'] = $query->contains('categories', $demand->getCategory());
        }

        if($demand->getZip()) {
            $constraints['zip'] = $query->like('zip', $demand->getZip().'%');
        }

        if (!empty($constraints)) {
            $query->matching(
                $query->logicalAnd($constraints)
            );
        }

        return $query->execute();
    }

    /**
     * Search for not disabled
     *
     * @return \TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findAllNotDisabled()
    {
        $query = $this->createQuery();
        $constraints = [];

        $constraints['hidden'] = $query->equals('hidden', 0);

        if (!empty($constraints)) {
            $query->matching(
                $query->logicalAnd($constraints)
            );
        }

        return $query->execute();
    }
}
