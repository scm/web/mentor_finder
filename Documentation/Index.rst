﻿.. include:: Includes.txt


.. _start:

=============
Mentor Finder
=============

.. only:: html

	:Classification:
		mentor_finder

	:Version:
		|release|

	:Language:
		de

	:Description:
		Diese Extension stellt eine Mentorensuche auf Basis einer Erweiterung der Tabelle fe_users zur Verfügung.

	:Keywords:
		Suche, Mentoren, Mentees, Kontakt

	:Copyright:
		Gesellschaft für Informatik e.V.

	:Author:
		Heiko Wollersheim

	:Email:
		heiko.wollersheim@eifel-online.com

	:License:
		This document is published under the Open Content License
		available from http://www.opencontent.org/opl.shtml

	:Rendered:
		|today|

	The content of this document is related to TYPO3,
	a GNU/GPL CMS/Framework available from `www.typo3.org <http://www.typo3.org/>`_.


	**Table of Contents**

.. toctree::
	:maxdepth: 5
	:titlesonly:
	:glob:

	Allgemein/Index
	Installation/Index
	Konfiguration/Index
	Versionen/Index
