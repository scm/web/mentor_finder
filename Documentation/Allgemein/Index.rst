﻿.. include:: ../Includes.txt

Beschreibung
============

Mit dieser Extension gibt man Mentees die Möglichkeit passende Mentoren über eine Listenansicht mit Suchfunktion zu finden und
diese direkt über ein Formular zu kontaktieren.

Die Mentoren werden auf Basis von FeUser-Datensätzen gepflegt. Alle dazu benötigten Felder werden durch die Extension erweitert.

Für die Registrierung der Mentoren wird die Extension sf_register verwendet und mit passenden Felder dieser Extension erweitert.

Alle Templates basieren auf Bootstrap, welches auch integriert ist und bei Bedarf eingebunden werden kann.


