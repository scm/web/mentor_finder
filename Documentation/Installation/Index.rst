﻿.. include:: ../Includes.txt

Installation
============

Die Extension kann wahlweise per Extension-Manager oder Composer installiert werden.
Anschließend muss nur noch das statische Template integriert und die Einstellungen über die Konstanten angepasst werden.