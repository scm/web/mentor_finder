﻿.. include:: ../Includes.txt

Konfiguration
=============

TypoScript
^^^^^^^^^^

Alle relevanten Einstellungen können über die Konstanten angepasst werden.

.. code-block:: typoscript

    plugin.tx_mentorfinder {
        settings {
            # cat=Mentor-Finder/100; type=boolean; label=Integrate Bootstrap CSS from CDN
            includeBootstrapCss = 1
            # cat=Mentor-Finder/100; type=boolean; label=Integrate Bootstrap JS (jQuery, Popper & Bootstrap) from CDN
            includeBootstrapJs = 1
            # cat=Mentor-Finder/100; type=boolean; label=Use ajax for forms and filters
            useAjax = 0
            # cat=Mentor-Finder/100; type=string; label=Templatefolder for Emails
            emailTemplateFolder = EXT:mentor_finder/Resources/Private/Templates/Email/
            # cat=Mentor-Finder/100; type=string; label=E-MAil-Address for System-Mails
            systemMailAddress = noreply@email.de
            # cat=Mentor-Finder/100; type=string; label=E-Mail-Address for Admin-Mails
            adminMailAddress = admin@email.de
            # cat=Mentor-Finder/100; type=string; label=E-Mail-Address for Report-Mails
            reportMailAddress = report@email.de
        }
        persistence {
            # cat=Mentor-Finder/100; type=int; label=StoragePid for Frontend-Users
            storagePid =
        }
    }

Einstellungen
^^^^^^^^^^^^^

==================================== ======================================== ===============
Property                             Title                                    Type
==================================== ======================================== ===============
includeExtJs                         Fügt JS mit Standardfunktionen hinzu     int
includeBootstrapCss                  Fügt CSS von Bootstrap ein               int
includeBootstrapJs                   Fügt JS von Bootstrap ein                int
useAjax                              AJAX für Listenansicht aktivieren        int
emailTemplateFolder                  Templateordner für die E-Mail Templates  string
systemMailAddress                    Ausgehende E-Mail Adresse                string
adminMailAddress                     Empfänger für Admin-Mails                string
reportMailAddress                    Empfänger für Gesuche                    string
==================================== ======================================== ===============
