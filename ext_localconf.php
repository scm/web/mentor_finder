<?php

use TYPO3\CMS\Core\Utility\GeneralUtility;
use GI\MentorFinder\Controller\SearchController;

defined('TYPO3_MODE') or die();

call_user_func(function () {

    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'GI.mentor_finder',
        'Search',
        [
            SearchController::class => 'list,form,send'
        ],
        // non-cacheable actions
        [
            SearchController::class => 'list,form,send'
        ]
    );

    GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\Container\Container::class)
        ->registerImplementation(\TYPO3\CMS\Extbase\Domain\Model\Category::class, \GI\MentorFinder\Domain\Model\Category::class);

    GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\Container\Container::class)
        ->registerImplementation(\Evoweb\SfRegister\Domain\Model\FrontendUser::class, \GI\MentorFinder\Domain\Model\FrontendUser::class);

    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][Evoweb\SfRegister\Controller\FeuserCreateController::class] = [
        'className' => GI\MentorFinder\Controller\FeuserCreateController::class
    ];

    $GLOBALS['TYPO3_CONF_VARS']['BE']['defaultUserTSconfig'] = '
    [GLOBAL]
    @import \'EXT:mentor_finder/Configuration/TypoScript/sf_register/setup.typoscript\'
    ' . $GLOBALS['TYPO3_CONF_VARS']['BE']['defaultUserTSconfig'];

    $GLOBALS['TYPO3_CONF_VARS']['SYS']['locallangXMLOverride']['de']['EXT:sf_register/Resources/Private/Language/locallang_be.xlf'][]
        = 'EXT:mentor_finder/Resources/Private/Language/locallang_db.xlf';

    $GLOBALS['TYPO3_CONF_VARS']['SYS']['locallangXMLOverride']['de']['EXT:sf_register/Resources/Private/Language/locallang.xlf'][]
        = 'EXT:mentor_finder/Resources/Private/Language/locallang.xlf';
});