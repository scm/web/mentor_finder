#
# Table structure for table 'fe_users'
#
CREATE TABLE fe_users (
    job_description tinytext,
    short_cv text,
    qualification tinytext,
    resources tinytext,
    requests smallint unsigned DEFAULT '0' NOT NULL,
    hidden int(1) unsigned DEFAULT '0' NOT NULL,
    categories int(11) unsigned DEFAULT '0' NOT NULL
);
