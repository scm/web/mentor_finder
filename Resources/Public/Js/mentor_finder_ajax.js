$(document).on('submit','#mentorFinder',function() {
    $('#mentorFinderContent').fadeTo( "slow", 0.33 );
    $.ajax({
        type: "POST",
        url: $(this).attr('action'),
        data: $(this).serialize(),
        success: function(data) {
            $('#mentorFinderContent').replaceWith($(data).find('#mentorFinderContent'));
            $('[data-toggle="popover"]').popover();
        }
    });
    return false;
});

$(document).on('click','#mentorFinderContent .ajax a',function(){
    $('#mentorFinderContent').fadeTo( "slow", 0.33 );
    $.ajax({
        url: $(this).attr('href'),
        dataType: 'text',
        success: function (data) {
            $('#mentorFinderContent').replaceWith($(data).find('#mentorFinderContent'));
            $('[data-toggle="popover"]').popover();
        }
    });
    return false;
});