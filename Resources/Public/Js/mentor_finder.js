$(document).on('hidden.bs.modal', "#modal", function(){
    $("#modal .modal-content").html("");
});

$(document).on('click', "a[data-toggle=mf_modal]", function(){
    $.ajax({
        url: $(this).attr('href'),
        dataType: 'text',
        success: function (data) {
            $("#modal .modal-content").html($(data).find('#mentorFinderContent'));
            $("#modal").modal("show");
        }
    });
    return false;
});

$(document).on('submit','#mentorFinderContact',function() {
    $.ajax({
        type: "POST",
        url: $(this).attr('action'),
        data: $(this).serialize(),
        success: function(data) {
            $("#modal .modal-content").html($(data).find('#mentorFinderContent'));
        }
    });
    return false;
});