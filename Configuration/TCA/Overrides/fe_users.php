<?php

$columns = [
    'job_description' => [
        'exclude' => 0,
        'label' => 'LLL:EXT:mentor_finder/Resources/Private/Language/locallang_db.xlf:fe_users.job_description',
        'config' => [
            'type' => 'input',
            'size' => 30
        ]
    ],
    'short_cv' => [
        'exclude' => 0,
        'label' => 'LLL:EXT:mentor_finder/Resources/Private/Language/locallang_db.xlf:fe_users.short_cv',
        'config' => [
            'type' => 'text',
            'cols' => 40,
            'rows' => 15,
            'eval' => 'trim',
            'wizards' => [
                'RTE' => [
                    'icon' => 'actions-wizard-rte',
                    'notNewRecords' => 1,
                    'RTEonly' => 1,
                    'module' => [
                        'name' => 'wizard_rte',
                    ],
                    'title' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:bodytext.W.RTE',
                    'type' => 'script'
                ]
            ]
        ],
        'defaultExtras' => 'richtext:rte_transform[mode=ts_css]',
    ],
    'qualification' => [
        'exclude' => 0,
        'label' => 'LLL:EXT:mentor_finder/Resources/Private/Language/locallang_db.xlf:fe_users.qualification',
        'config' => [
            'type' => 'input',
            'size' => 100
        ]
    ],
    'resources' => [
        'exclude' => 0,
        'label' => 'LLL:EXT:mentor_finder/Resources/Private/Language/locallang_db.xlf:fe_users.resources',
        'config' => [
            'type' => 'input',
            'size' => 100
        ]
    ],
    'requests' => [
        'exclude' => 0,
        'label' => 'LLL:EXT:mentor_finder/Resources/Private/Language/locallang_db.xlf:fe_users.requests',
        'config' => [
            'type' => 'input',
            'size' => 20
        ]
    ],
    'hidden' => [
        'exclude' => 1,
        'label' => 'LLL:EXT:mentor_finder/Resources/Private/Language/locallang_db.xlf:fe_users.hidden',
        'config' => [
            'type' => 'check'
        ]
    ],
    'categories' => [
        'config' => [
            'type' => 'category'
        ]
    ]
];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns(
    'fe_users',
    $columns
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'fe_users',
    '--div--;LLL:EXT:mentor_finder/Resources/Private/Language/locallang_db.xlf:mentor_finder,job_description,short_cv,qualification,resources,requests,hidden,categories'
);