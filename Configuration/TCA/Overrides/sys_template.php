<?php

defined('TYPO3_MODE') || die();

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    'mentor_finder',
    'Configuration/TypoScript/mentor_finder',
    'Mentor Finder Konfiguration'
);

