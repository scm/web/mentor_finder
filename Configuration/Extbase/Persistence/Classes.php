<?php
declare(strict_types = 1);
return [
    \GI\MentorFinder\Domain\Model\FrontendUser::class => [
        'tableName' => 'fe_users',
    ],
    \GI\MentorFinder\Domain\Model\Category::class => [
        'tableName' => 'sys_category',
    ],
];